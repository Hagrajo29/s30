//npm init -y -skips all the prompt and goes directly into installing npm/ creating package.json

const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3000;


mongoose.connect("mongodb+srv://Hagrajo9:GwapoKo23@cluster0.rw0wp.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;
db.on("error", console.error.bind(console,"Connection Error"));
//if the connection is successful
db.once("open", () => console.log("We're connected to the database"));




app.use(express.json());
app.use(express.urlencoded({ extended:true }));

// Mongoose Schema - sets the structure of the document that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
	name:String,
	status: {
		type:String,
		//default - sets the value once the filed does not have any value entered in it
		default: "pending"
	}
})

//model - allows access to methods that will perform CRUD operations in the database; RULE: the first letter is always capital/uppercase for the variable of the model and must be singular form
/*SYNTAX:
const <Variable> = mongoose.model ("<Variable>", <schemaName>)
*/
const Task = mongoose.model("Task", taskSchema)


//routes
/*
business logic:
1. check if the task is already existing
	if the task exists, return "there is a duplicate task"
	if it is not existing, add the task in the database


2. the task will come from the request body

3. create a new task object with the needed properties


4. save to db
*/

app.post("/tasks", (req, res) => {
	//checking for duplicate tasks
	//findOne is a mongoose method that acts similar to find in MongoDB; returns the first document it finds
	Task.findOne({name:req.body.name}, (error,result) => {
		console.log(req.body.name)
		//if the "result" has found a task, it will return the res.send
		if(result !== null && result.name === req.body.name) {
			return res.send("There is a dupplicate task");
		}else{
			//if no task is found, create the object and save it to the db
			let newTask = new Task ({
				name: req.body.name
			})
			//saveErr - parameter that accepts errors, should there be any when saving the "newTask" object
			//savedTask- parameter that accepts the object should the saving of the "newTask" object is a success
			newTask.save((saveErr, savedTask) => {
				if (saveErr){
					return console.error(saveErr);
				}else{
					//.status - returns a status (number code such as 201 for successful creation)
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

/*
1. retrieve all the documents using the find() functionality
2. if an error is encountered, print the error (error handling)
3. if there are no errors, send a success status back to the client and return an array of documents
same /tasks route

*/
app.get("/tasks", (req, res) => {
	//fins is similar to the mongodb/robo3t find. setting up empty field in "{}" would allow the app to find all documents inside the database
	Task.find({}, (error, result) =>{
		if(error){
			return console.log(error)
		}else{
			return res.status(200).json({data:result})
			//data field will be set and its value will be the result/response that we had for the request
		}
	})
})

// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
	- if the user is existing, return "username already in use"
	- if the user is not existing, add it in the database
	 	- if the username and the password are filled, save
			 - in saving, create a new User object (a User schema)
				- if there is an errror, log it in the console
				- if there is no error, send a status of 201 and "successfully registered"
		- if one of them is blank, send the response "BOTH username and password must be provided"
*/

const userSchema = new mongoose.Schema ({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema)

app.post("/register", (req, res) => {
	
	User.find({username:req.body.username, password:req.body.password}, (error,result) => {
		console.log(req.body.username)
		console.log(req.body.password)
		if(result !== null && result.username === req.body.username || result !== null && result.password === req.body.password) {
			return res.send("Username already in use.");

		}

		if(result === null && result.username === null || result === null && result.password === null) {
			return res.send("Both username and password must be provided.");

		}else{

			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})
			
			newUser.save((saveErr, savedUser) => {
				if (saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("Successfully registered.")
				}
			})
		}
	})
})

// create a get request for getting all registered users
/*
	- retrieve all users registerd using find functionality
	- if there is an error, log it in the console
	- if there is no error, send a success status back to the client and return the array of users
*/

app.get("/user", (req, res) => {
	User.find({}, (error, result) =>{
		if(error){
			return console.log(error)
		}else{
			return res.status(200).json({data:result})
		}
	})
})



app.listen(port, () => console.log(`Server is running at port ${port}`));

